function nav() {
    var nav = document.getElementById('nav');
    var nav2 = document.getElementById('nav2');
    if (nav.classList.contains("lg:max-w-[60px]")) {
        nav.classList.remove("lg:max-w-[60px]");
        nav.classList.add("max-w-1000]");
        nav2.classList.remove("-translate-x-full");
        document.getElementById("nav").style.transition = "all 0.5s";
    } else {
        nav.classList.remove("max-w-1000");
        nav.classList.add("lg:max-w-[60px]");
        nav2.classList.add("-translate-x-full");
        document.getElementById("nav").style.transition = "all 0.5s";
    }
}